namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    public interface IMNKConsoleControl
    {
        IMNKMatch Model { get; set; }

        void Input();

        void MakeMove(int i, int j);
    }
}