﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    public static class Program
    {
        const string valueStr = "SomeString";
        const int valueInt = 17;
        readonly static DateTime valueDate = new DateTime(2018, 12, 24);

        public static void Main(string[] args)
        {
            var t1 = Tuple.Of(valueStr);
            var t2 = Tuple.Of(valueInt, valueStr);
            var t3 = Tuple.Of(valueDate, valueStr, valueInt);

            Assert(t1 is ITuple<string>);
            Assert(t1.Length == 1);
            Assert(t1.First == valueStr);
            Assert(Enumerable.SequenceEqual(t1.ToArray(), new object[] { valueStr }));

            Assert(t2 is ITuple<int, string>);
            Assert(t2.Length == 2);
            Assert(t2.First == valueInt);
            Assert(t2.Second == valueStr);
            Assert(Enumerable.SequenceEqual(t2.ToArray(), new object[] { valueInt, valueStr }));

            Assert(t3 is ITuple<DateTime, string, int>);
            Assert(t3.Length == 3);
            Assert(t3.First == valueDate);
            Assert(t3.Second == valueStr);
            Assert(t3.Third == valueInt);
            Assert(Enumerable.SequenceEqual(t3.ToArray(), new object[] { valueDate, valueStr, valueInt }));

            var r1 = Tuple.Of(valueStr);
            var r2 = Tuple.Of(valueInt, valueStr);
            var r3 = Tuple.Of(valueDate, valueStr, valueInt);

            Assert(t1.Equals(r1));
            Assert(t2.Equals(r2));
            Assert(t3.Equals(r3));

            Assert(t1.GetHashCode() == r1.GetHashCode());
            Assert(t2.GetHashCode() == r2.GetHashCode());
            Assert(t3.GetHashCode() == r3.GetHashCode());

            Assert(t1.ToString() == $"(" + valueStr + ")");
            Assert(t2.ToString() == string.Format("({0}, {1})", valueInt, valueStr));
            Assert(t3.ToString() == $"({valueDate}, {valueStr}, {valueInt})");

            Console.WriteLine("OK");
            Console.ReadLine();
        }

        private static void Assert(bool expr, string msg = "Assertion failed")
        {
            if (!expr)
            {
                throw new Exception(msg);
            }
        }
    }
}
