﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly object[] items;

        public TupleImpl(object[] args)
        {
            this.items = args;
        }

        public object get(int index)
        {
            return items[index];
        }

        public int getSize()
        {
            return items.Length;
        }
            public object[] ToArray()
        {
            object[] i = new object[this.getSize()];
            Array.Copy(items, i, this.getSize());
            return i;
        }
        public override string ToString()
        {
            //return "(" + items.AsEnumerable()
            //        .Select(item => item.ToString())
            //        .Aggregate((a, b) => a + ", " + b) + ")";
            return "(" + string.Join(", ", items) + ")";
        }

        public object this[int i] => ToArray().ElementAt(i);

        public int Length => getSize();
        override public int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            foreach(object obj in items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        
         public override bool Equals(object obj)
        { 
            return obj != null 
                && obj is TupleImpl 
                && GetHashCode() == ((TupleImpl)obj).GetHashCode();
        }


    }
}

